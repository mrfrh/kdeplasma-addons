# Albanian translation for kdeplasma-addons
# Copyright (c) 2009 Rosetta Contributors and Canonical Ltd 2009
# This file is distributed under the same license as the kdeplasma-addons package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-07 00:48+0000\n"
"PO-Revision-Date: 2010-03-08 14:42+0000\n"
"Last-Translator: Vilson Gjeci <vilsongjeci@gmail.com>\n"
"Language-Team: Albanian <sq@li.org>\n"
"Language: sq\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2011-04-21 23:54+0000\n"
"X-Generator: Launchpad (build 12883)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr ""

#: package/contents/ui/configAppearance.qml:41
#, kde-format
msgctxt "@label:spinbox"
msgid "Grid size:"
msgstr ""

#: package/contents/ui/configAppearance.qml:50
#, kde-format
msgid "Background:"
msgstr ""

#: package/contents/ui/configAppearance.qml:56
#, kde-format
msgid "Color:"
msgstr ""

#: package/contents/ui/configAppearance.qml:71
#, kde-format
msgid "Image:"
msgstr ""

#: package/contents/ui/configAppearance.qml:79
#, fuzzy, kde-format
#| msgid "Use custom image:"
msgctxt "@info:placeholder"
msgid "Path to custom image…"
msgstr "Përdor imazh të vetëzgjedhur:"

#: package/contents/ui/configAppearance.qml:98
#, kde-format
msgctxt "@info:tooltip"
msgid "Choose image…"
msgstr ""

#: package/contents/ui/configAppearance.qml:106
#, kde-format
msgctxt "@title:window"
msgid "Choose an Image"
msgstr ""

#: package/contents/ui/configAppearance.qml:111
#, kde-format
msgid "Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz)"
msgstr ""

#: package/contents/ui/configAppearance.qml:125
#, kde-format
msgid "Tiles:"
msgstr ""

#: package/contents/ui/configAppearance.qml:129
#, kde-format
msgid "Colored numbers:"
msgstr ""

#: package/contents/ui/FifteenPuzzle.qml:258
#, kde-format
msgctxt "The time since the puzzle started, in minutes and seconds"
msgid "Time: %1"
msgstr ""

#: package/contents/ui/FifteenPuzzle.qml:301
#, fuzzy, kde-format
#| msgid "Shuffle Pieces"
msgctxt "@action:button"
msgid "Shuffle"
msgstr "Ndrysho Copëzat"

#: package/contents/ui/FifteenPuzzle.qml:338
#, kde-format
msgctxt "@info"
msgid "Solved! Try again."
msgstr ""

#: package/contents/ui/main.qml:25
#, fuzzy, kde-format
#| msgid "Configure Fifteen Puzzle"
msgid "Fifteen Puzzle"
msgstr "Konfiguro Fifteen Puzzle"

#: package/contents/ui/main.qml:26
#, kde-format
msgid "Solve by arranging in order"
msgstr ""

#, fuzzy
#~| msgid "Size:"
#~ msgctxt "@label:spinbox"
#~ msgid "Size:"
#~ msgstr "Përmasa:"

#, fuzzy
#~| msgid "Use custom image:"
#~ msgctxt "@option:check"
#~ msgid "Use custom image"
#~ msgstr "Përdor imazh të vetëzgjedhur:"

#, fuzzy
#~| msgid "Show numerals:"
#~ msgctxt "@option:check"
#~ msgid "Show numerals"
#~ msgstr "Shfaq numrat:"

#~ msgid "General"
#~ msgstr "Të Përgjithshme"

#~ msgid " pieces wide"
#~ msgstr " gjerësia e copave"

#~ msgid "Use plain pieces:"
#~ msgstr "Përdor copa të sheshta:"
