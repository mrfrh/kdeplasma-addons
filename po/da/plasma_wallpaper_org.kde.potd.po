# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Martin Schlander <mschlander@opensuse.org>, 2018, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-19 00:47+0000\n"
"PO-Revision-Date: 2020-01-30 20:25+0100\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <kde-i18n-doc@kde.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.3\n"

#: package/contents/ui/config.qml:48
#, kde-format
msgctxt "@label:listbox"
msgid "Provider:"
msgstr "Udbyder:"

#: package/contents/ui/config.qml:66
#, kde-format
msgctxt "@option:check"
msgid "Update when using metered network connection"
msgstr ""

#: package/contents/ui/config.qml:76
#, kde-format
msgctxt "@label:listbox"
msgid "Category:"
msgstr "Kategori:"

#: package/contents/ui/config.qml:80
#, kde-format
msgctxt "@item:inlistbox"
msgid "All"
msgstr "Alle"

#: package/contents/ui/config.qml:84
#, kde-format
msgctxt "@item:inlistbox"
msgid "1080p"
msgstr "1080p"

#: package/contents/ui/config.qml:88
#, kde-format
msgctxt "@item:inlistbox"
msgid "4K"
msgstr "4K"

#: package/contents/ui/config.qml:92
#, kde-format
msgctxt "@item:inlistbox"
msgid "Ultra Wide"
msgstr "Ultra Wide"

#: package/contents/ui/config.qml:96
#, kde-format
msgctxt "@item:inlistbox"
msgid "Background"
msgstr "Baggrund"

#: package/contents/ui/config.qml:100
#, kde-format
msgctxt "@item:inlistbox"
msgid "Lock Screen"
msgstr "Låseskærm"

#: package/contents/ui/config.qml:104
#, kde-format
msgctxt "@item:inlistbox"
msgid "Nature"
msgstr "Nature"

#: package/contents/ui/config.qml:108
#, kde-format
msgctxt "@item:inlistbox"
msgid "Tumblr"
msgstr "Tumblr"

#: package/contents/ui/config.qml:112
#, kde-format
msgctxt "@item:inlistbox"
msgid "Black"
msgstr "Sort"

#: package/contents/ui/config.qml:116
#, kde-format
msgctxt "@item:inlistbox"
msgid "Flower"
msgstr "Blomst"

#: package/contents/ui/config.qml:120
#, kde-format
msgctxt "@item:inlistbox"
msgid "Funny"
msgstr "Sjov"

#: package/contents/ui/config.qml:124
#, kde-format
msgctxt "@item:inlistbox"
msgid "Cute"
msgstr "Nuttet"

#: package/contents/ui/config.qml:128
#, kde-format
msgctxt "@item:inlistbox"
msgid "Cool"
msgstr "Cool"

#: package/contents/ui/config.qml:132
#, kde-format
msgctxt "@item:inlistbox"
msgid "Fall"
msgstr "Efterår"

#: package/contents/ui/config.qml:136
#, kde-format
msgctxt "@item:inlistbox"
msgid "Love"
msgstr "Kærlighed"

#: package/contents/ui/config.qml:140
#, kde-format
msgctxt "@item:inlistbox"
msgid "Design"
msgstr "Design"

#: package/contents/ui/config.qml:144
#, kde-format
msgctxt "@item:inlistbox"
msgid "Christmas"
msgstr "Jul"

#: package/contents/ui/config.qml:148
#, kde-format
msgctxt "@item:inlistbox"
msgid "Travel"
msgstr "Rejse"

#: package/contents/ui/config.qml:152
#, kde-format
msgctxt "@item:inlistbox"
msgid "Beach"
msgstr "Strand"

#: package/contents/ui/config.qml:156
#, kde-format
msgctxt "@item:inlistbox"
msgid "Car"
msgstr "Bil"

#: package/contents/ui/config.qml:160
#, kde-format
msgctxt "@item:inlistbox"
msgid "Sports"
msgstr "Sport"

#: package/contents/ui/config.qml:164
#, kde-format
msgctxt "@item:inlistbox"
msgid "Animal"
msgstr "Dyr"

#: package/contents/ui/config.qml:168
#, kde-format
msgctxt "@item:inlistbox"
msgid "People"
msgstr "Mennesker"

#: package/contents/ui/config.qml:172
#, kde-format
msgctxt "@item:inlistbox"
msgid "Music"
msgstr "Musik"

#: package/contents/ui/config.qml:176
#, kde-format
msgctxt "@item:inlistbox"
msgid "Summer"
msgstr "Sommer"

#: package/contents/ui/config.qml:180
#, kde-format
msgctxt "@item:inlistbox"
msgid "Galaxy"
msgstr "Galakse"

#: package/contents/ui/config.qml:184
#, kde-format
msgctxt "@item:inlistbox"
msgid "Tabliss"
msgstr ""

#: package/contents/ui/config.qml:214
#, kde-format
msgctxt "@label:listbox"
msgid "Positioning:"
msgstr "Positionering:"

#: package/contents/ui/config.qml:217
#, kde-format
msgctxt "@item:inlistbox"
msgid "Scaled and Cropped"
msgstr "Skaleret og beskåret"

#: package/contents/ui/config.qml:221
#, kde-format
msgctxt "@item:inlistbox"
msgid "Scaled"
msgstr "Skaleret"

#: package/contents/ui/config.qml:225
#, kde-format
msgctxt "@item:inlistbox"
msgid "Scaled, Keep Proportions"
msgstr "Skaleret, behold proportioner"

#: package/contents/ui/config.qml:229
#, kde-format
msgctxt "@item:inlistbox"
msgid "Centered"
msgstr "Centreret"

#: package/contents/ui/config.qml:233
#, kde-format
msgctxt "@item:inlistbox"
msgid "Tiled"
msgstr "Fliselagt"

#: package/contents/ui/config.qml:254
#, kde-format
msgctxt "@label:chooser"
msgid "Background color:"
msgstr "Baggrundsfarve:"

#: package/contents/ui/config.qml:255
#, kde-format
msgctxt "@title:window"
msgid "Select Background Color"
msgstr "Vælg baggrundsfarve"

#: package/contents/ui/config.qml:266
#, kde-format
msgctxt "@label"
msgid "Today's picture:"
msgstr ""

#: package/contents/ui/config.qml:282
#, kde-format
msgctxt "@label"
msgid "Title:"
msgstr ""

#: package/contents/ui/config.qml:297
#, kde-format
msgctxt "@label"
msgid "Author:"
msgstr ""

#: package/contents/ui/config.qml:317
#, kde-format
msgctxt "@action:button"
msgid "Open Containing Folder"
msgstr ""

#: package/contents/ui/config.qml:321
#, kde-format
msgctxt "@info:whatsthis for a button"
msgid "Open the destination folder where the wallpaper image was saved."
msgstr ""

#: package/contents/ui/WallpaperDelegate.qml:142
#, kde-format
msgctxt "@info:whatsthis"
msgid "Today's picture"
msgstr ""

#: package/contents/ui/WallpaperDelegate.qml:143
#, kde-format
msgctxt "@info:whatsthis"
msgid "Loading"
msgstr ""

#: package/contents/ui/WallpaperDelegate.qml:144
#, kde-format
msgctxt "@info:whatsthis"
msgid "Unavailable"
msgstr ""

#: package/contents/ui/WallpaperDelegate.qml:145
#, kde-format
msgctxt "@info:whatsthis for an image %1 title %2 author"
msgid "%1 Author: %2. Right-click on the image to see more actions."
msgstr ""

#: package/contents/ui/WallpaperDelegate.qml:146
#, kde-format
msgctxt "@info:whatsthis"
msgid "The wallpaper is being fetched from the Internet."
msgstr ""

#: package/contents/ui/WallpaperDelegate.qml:147
#, kde-format
msgctxt "@info:whatsthis"
msgid "Failed to fetch the wallpaper from the Internet."
msgstr ""

#: package/contents/ui/WallpaperPreview.qml:50
#, kde-format
msgctxt "@action:inmenu wallpaper preview menu"
msgid "Save Image as…"
msgstr ""

#: package/contents/ui/WallpaperPreview.qml:53
#, kde-format
msgctxt "@info:whatsthis for a button and a menu item"
msgid "Save today's picture to local disk"
msgstr ""

#: package/contents/ui/WallpaperPreview.qml:59
#, kde-format
msgctxt ""
"@action:inmenu wallpaper preview menu, will open the website of the wallpaper"
msgid "Open Link in Browser…"
msgstr ""

#: package/contents/ui/WallpaperPreview.qml:62
#, kde-format
msgctxt "@info:whatsthis for a menu item"
msgid "Open the website of today's picture in the default browser"
msgstr ""

#: plugins/potdbackend.cpp:219
#, kde-format
msgctxt "@title:window"
msgid "Save Today's Picture"
msgstr ""

#: plugins/potdbackend.cpp:221
#, kde-format
msgctxt "@label:listbox Template for file dialog"
msgid "JPEG image (*.jpeg *.jpg *.jpe)"
msgstr ""

#: plugins/potdbackend.cpp:238
#, kde-format
msgctxt "@info:status after a save action"
msgid "The image was not saved."
msgstr ""

#: plugins/potdbackend.cpp:244
#, kde-format
msgctxt "@info:status after a save action %1 file path %2 basename"
msgid "The image was saved as <a href=\"%1\">%2</a>"
msgstr ""
