# translation of plasma_applet_notes.po to Finnish
# translation of plasma_applet_notes.po to
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Mikko Piippo <piippo@cc.helsinki.fi>, 2008.
# Teemu Rytilahti <teemu.rytilahti@kde-fi.org>, 2008.
# Teemu Rytilahti <teemu.rytilahti@d5k.net>, 2008.
# Tommi Nieminen <translator@legisign.org>, 2010, 2018, 2019, 2021.
# Lasse Liehu <lasse.liehu@gmail.com>, 2011, 2014, 2015.
#
# KDE Finnish translation sprint participants:
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_notes\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-01-05 00:49+0000\n"
"PO-Revision-Date: 2021-05-06 11:05+0300\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-POT-Import-Date: 2012-12-01 22:25:24+0000\n"
"X-Generator: Lokalize 20.04.2\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Ulkoasu"

#: package/contents/ui/configAppearance.qml:30
#, kde-format
msgid "%1pt"
msgstr "%1 pt"

#: package/contents/ui/configAppearance.qml:36
#, kde-format
msgid "Text font size:"
msgstr "Tekstifontin koko:"

#: package/contents/ui/configAppearance.qml:41
#, kde-format
msgid "Background color"
msgstr "Taustaväri"

#: package/contents/ui/configAppearance.qml:77
#, kde-format
msgid "A white sticky note"
msgstr "Valkoinen muistilappu"

#: package/contents/ui/configAppearance.qml:78
#, kde-format
msgid "A black sticky note"
msgstr "Musta muistilappu"

#: package/contents/ui/configAppearance.qml:79
#, kde-format
msgid "A red sticky note"
msgstr "Punainen muistilappu"

#: package/contents/ui/configAppearance.qml:80
#, kde-format
msgid "An orange sticky note"
msgstr "Oranssi muistilappu"

#: package/contents/ui/configAppearance.qml:81
#, kde-format
msgid "A yellow sticky note"
msgstr "Keltainen muistilappu"

#: package/contents/ui/configAppearance.qml:82
#, kde-format
msgid "A green sticky note"
msgstr "Vihreä muistilappu"

#: package/contents/ui/configAppearance.qml:83
#, kde-format
msgid "A blue sticky note"
msgstr "Sininen muistilappu"

#: package/contents/ui/configAppearance.qml:84
#, kde-format
msgid "A pink sticky note"
msgstr "Vaaleanpunainen muistilappu"

#: package/contents/ui/configAppearance.qml:85
#, kde-format
msgid "A translucent sticky note"
msgstr "Läpikuultava muistilappu"

#: package/contents/ui/configAppearance.qml:86
#, kde-format
msgid "A translucent sticky note with light text"
msgstr "Läpikuultava muistilappu vaalealla tekstillä"

#: package/contents/ui/main.qml:255
#, kde-format
msgid "Undo"
msgstr "Kumoa"

#: package/contents/ui/main.qml:263
#, kde-format
msgid "Redo"
msgstr "Tee uudelleen"

#: package/contents/ui/main.qml:273
#, kde-format
msgid "Cut"
msgstr "Leikkaa"

#: package/contents/ui/main.qml:281
#, kde-format
msgid "Copy"
msgstr "Kopioi"

#: package/contents/ui/main.qml:289
#, kde-format
msgid "Paste"
msgstr "Liitä"

#: package/contents/ui/main.qml:295
#, fuzzy, kde-format
#| msgid "Paste Without Formatting"
msgid "Paste with Full Formatting"
msgstr "Liitä muotoiluitta"

#: package/contents/ui/main.qml:302
#, fuzzy, kde-format
#| msgid "Formatting"
msgctxt "@action:inmenu"
msgid "Remove Formatting"
msgstr "Muotoilu"

#: package/contents/ui/main.qml:317
#, kde-format
msgid "Delete"
msgstr "Poista"

#: package/contents/ui/main.qml:324
#, kde-format
msgid "Clear"
msgstr "Tyhjennä"

#: package/contents/ui/main.qml:334
#, kde-format
msgid "Select All"
msgstr "Valitse kaikki"

#: package/contents/ui/main.qml:462
#, kde-format
msgctxt "@info:tooltip"
msgid "Bold"
msgstr "Lihavointi"

#: package/contents/ui/main.qml:474
#, kde-format
msgctxt "@info:tooltip"
msgid "Italic"
msgstr "Kursivointi"

#: package/contents/ui/main.qml:486
#, kde-format
msgctxt "@info:tooltip"
msgid "Underline"
msgstr "Alleviivaus"

#: package/contents/ui/main.qml:498
#, kde-format
msgctxt "@info:tooltip"
msgid "Strikethrough"
msgstr "Yliviivaus"

#: package/contents/ui/main.qml:572
#, kde-format
msgid "Discard this note?"
msgstr "Hylätäänkö tämä muistiinpano?"

#: package/contents/ui/main.qml:573
#, kde-format
msgid "Are you sure you want to discard this note?"
msgstr "Haluatko varmasti hylätä tämän muistiinpanon?"

#: package/contents/ui/main.qml:586
#, kde-format
msgctxt "@item:inmenu"
msgid "White"
msgstr "Valkoinen"

#: package/contents/ui/main.qml:587
#, kde-format
msgctxt "@item:inmenu"
msgid "Black"
msgstr "Musta"

#: package/contents/ui/main.qml:588
#, kde-format
msgctxt "@item:inmenu"
msgid "Red"
msgstr "Punainen"

#: package/contents/ui/main.qml:589
#, kde-format
msgctxt "@item:inmenu"
msgid "Orange"
msgstr "Oranssi"

#: package/contents/ui/main.qml:590
#, kde-format
msgctxt "@item:inmenu"
msgid "Yellow"
msgstr "Keltainen"

#: package/contents/ui/main.qml:591
#, kde-format
msgctxt "@item:inmenu"
msgid "Green"
msgstr "Vihreä"

#: package/contents/ui/main.qml:592
#, kde-format
msgctxt "@item:inmenu"
msgid "Blue"
msgstr "Sininen"

#: package/contents/ui/main.qml:593
#, kde-format
msgctxt "@item:inmenu"
msgid "Pink"
msgstr "Vaaleanpunainen"

#: package/contents/ui/main.qml:594
#, kde-format
msgctxt "@item:inmenu"
msgid "Translucent"
msgstr "Läpikuultava"

#: package/contents/ui/main.qml:595
#, kde-format
msgctxt "@item:inmenu"
msgid "Translucent Light"
msgstr "Vaalea läpikuultava"

#~ msgid "Toggle text format options"
#~ msgstr "Näytä tai piilota tekstin muotoilun valinnat"

#~ msgid "Notes Settings..."
#~ msgstr "Muistilapun asetukset…"

#~ msgid "Align left"
#~ msgstr "Tasaa vasemmalle"

#~ msgid "Align center"
#~ msgstr "Tasaa keskelle"

#~ msgid "Align right"
#~ msgstr "Tasaa oikealle"

#~ msgid "Justified"
#~ msgstr "Tasaa molemmat reunat"

#~ msgid "Font"
#~ msgstr "Fontti"

#~ msgid "Style:"
#~ msgstr "Tyyli:"

#~ msgid "&Bold"
#~ msgstr "&Lihavoitu"

#~ msgid "&Italic"
#~ msgstr "&Kursiivi"

#~ msgid "Size:"
#~ msgstr "Koko:"

#~ msgid "Scale font size by:"
#~ msgstr "Suhteuta fonttikokoa:"

#~ msgid "%"
#~ msgstr " %"

#~ msgid "Color:"
#~ msgstr "Väri:"

#~ msgid "Use theme color"
#~ msgstr "Käytä teeman väriä"

#~ msgid "Use custom color:"
#~ msgstr "Käytä mukautettua väriä:"

#~ msgid "Active line highlight color:"
#~ msgstr "Aktiivisen rivin korostusväri:"

#~ msgid "Use no color"
#~ msgstr "Älä käytä väriä"

#~ msgid "Theme"
#~ msgstr "Teema"

#~ msgid "Notes color:"
#~ msgstr "Muistilapun väri:"

#~ msgid "Spell Check"
#~ msgstr "Oikoluku"

#~ msgid "Enable spell check:"
#~ msgstr "Käytä oikolukua:"

#~ msgid "Notes Color"
#~ msgstr "Muistilapun väri"

#~ msgid "General"
#~ msgstr "Yleistä"

#~ msgid "Unable to open file"
#~ msgstr "Ei voitu avata tiedostoa"

#, fuzzy
#~| msgid "Welcome to the Notes Plasmoid! Type your notes here..."
#~ msgid "Welcome to the Notes Plasmoid! Type your notes here..."
#~ msgstr ""
#~ "Tervetuloa käyttämään muistilappuja. Kirjoita muistiinpanosi tähän..."

#, fuzzy
#~| msgid "Color:"
#~ msgid "Text Color"
#~ msgstr "Väri:"

#, fuzzy
#~| msgid "Font:"
#~ msgid "Font style:"
#~ msgstr "Kirjasin:"

#, fuzzy
#~| msgid "Font:"
#~ msgid "Font size:"
#~ msgstr "Kirjasin:"

#~ msgid "Notes Configuration"
#~ msgstr "Muistilappujen asetukset"
