# translation of krunner_converterrunner.po to Galician
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# mvillarino <mvillarino@users.sourceforge.net>, 2008.
# Marce Villarino <mvillarino@gmail.com>, 2009, 2014.
# Adrian Chaves Fernandez <adriyetichaves@gmail.com>, 2013, 2017.
msgid ""
msgstr ""
"Project-Id-Version: krunner_converterrunner\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-09 00:47+0000\n"
"PO-Revision-Date: 2017-06-10 14:58+0100\n"
"Last-Translator: Adrián Chaves Fernández (Gallaecio) <adriyetichaves@gmail."
"com>\n"
"Language-Team: Galician <kde-i18n-doc@kde.org>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: converterrunner.cpp:33
#, kde-format
msgid ""
"Converts the value of :q: when :q: is made up of \"value unit [>, to, as, "
"in] unit\". You can use the Unit converter applet to find all available "
"units."
msgstr ""
"Converte o valor de :q: cando :q: está formado por «valor unidade [>, →, a, "
"como, en] unidade». Pode usar o miniaplicativo de conversión de unidades "
"para atopar todas as unidades dispoñíbeis."

#: converterrunner.cpp:42
#, kde-format
msgctxt "list of words that can used as amount of 'unit1' [in|to|as] 'unit2'"
msgid "in;to;as"
msgstr ">;→;a;como;en"

#: converterrunner.cpp:50
#, kde-format
msgid "Copy unit and number"
msgstr ""
