# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
#
# Giovanni Sora <g.sora@tiscali.it>, 2019, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-31 00:48+0000\n"
"PO-Revision-Date: 2022-07-15 11:44+0200\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Dictionaries"
msgstr "Dictionarios"

#: package/contents/ui/AvailableDictSheet.qml:35
#, kde-format
msgid "Add More Dictionaries"
msgstr "Adde altere Dictionarios"

#: package/contents/ui/ConfigDictionaries.qml:89
#, kde-format
msgid "Unable to load dictionary list"
msgstr "Il non pote cargar lista de dictionario"

#: package/contents/ui/ConfigDictionaries.qml:90
#: package/contents/ui/main.qml:121
#, kde-format
msgctxt "%2 human-readable error string"
msgid "Error code: %1 (%2)"
msgstr "Codice de error: %1 (%2)"

#: package/contents/ui/ConfigDictionaries.qml:101
#, kde-format
msgid "No dictionaries"
msgstr "Nulle Dictionarios"

#: package/contents/ui/ConfigDictionaries.qml:112
#, kde-format
msgid "Add More…"
msgstr "Adde altere ..."

#: package/contents/ui/DictItemDelegate.qml:51
#, kde-format
msgid "Delete"
msgstr "Dele"

#: package/contents/ui/main.qml:40
#, kde-format
msgctxt "@info:placeholder"
msgid "Enter word to define here…"
msgstr "Inserta parola de definir hic…"

#: package/contents/ui/main.qml:120
#, kde-format
msgid "Unable to load definition"
msgstr "Incapace a cargar definition"

#~ msgid "Looking up definition…"
#~ msgstr "Cercante definition ..."
